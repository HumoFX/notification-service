import pytest
from fastapi import FastAPI
from httpx import AsyncClient
from starlette import status


@pytest.mark.anyio
async def test_health(client: AsyncClient, fastapi_app: FastAPI) -> None:
    """
    Checks the health endpoint.

    :param client: client for the app.
    :param fastapi_app: current FastAPI application.
    """
    url = fastapi_app.url_path_for("health_check")
    response = await client.get(url)
    assert response.status_code == status.HTTP_200_OK


@pytest.mark.anyio
async def test_create_notification(client: AsyncClient, fastapi_app: FastAPI) -> None:
    """
    Test create notification.

    :param client: client for the app.
    :param fastapi_app: current FastAPI application.
    """
    url = fastapi_app.url_path_for("create_notification")
    data = {
        "message": "Test message",
        "start_at": "2023-09-10 00:00",
        "end_at": "2023-09-11 00:00",
        "filtering": {
            "tag": "test_tag",
            "phone_code": "test_phone_code",
        },
    }
    response = await client.post(url, json=data)
    assert response.status_code == status.HTTP_200_OK
    assert response.json()["message"] == "Test message"
    assert response.json()["start_at"] == "2021-09-10 00:00"
    assert response.json()["end_at"] == "2021-09-11 00:00"
    assert response.json()["filtering"]["tag"] == "test_tag"
    assert response.json()["filtering"]["phone_code"] == "test_phone_code"
    assert response.json()["status"] == "created"
    assert response.json()["created_at"] is not None
    assert response.json()["updated_at"] is not None
    assert response.json()["id"] is not None
