"""Echo API."""
from notification_service.web.api.echo.views import router

__all__ = ["router"]
