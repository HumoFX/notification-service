"""Redis API."""
from notification_service.web.api.redis.views import router

__all__ = ["router"]
