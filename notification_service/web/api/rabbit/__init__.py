"""API to interact with RabbitMQ."""
from notification_service.web.api.rabbit.views import router

__all__ = ["router"]
