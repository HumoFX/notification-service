"""API for checking project status."""
from notification_service.web.api.monitoring.views import router

__all__ = ["router"]
