from fastapi import APIRouter

from notification_service.web.api.v1.client.views import router as client_router
from notification_service.web.api.v1.notification.views import (
    router as notification_router,
)

router = APIRouter(prefix="/v1", tags=["v1"])
router.include_router(client_router)
router.include_router(notification_router)
