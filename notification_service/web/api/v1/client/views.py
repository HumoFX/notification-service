from fastapi import APIRouter
from fastapi.param_functions import Depends
from fastapi.responses import JSONResponse
from redis.asyncio import ConnectionPool, Redis
from sqlalchemy.exc import SQLAlchemyError

from notification_service.db.dependencies import get_db_session
from notification_service.db.models import Client, Notification, NotificationReceiver
from notification_service.logging import logger
from notification_service.services.redis.dependency import get_redis_pool
from notification_service.web.api.v1.client.schema import (
    ClientCreateDTO,
    ClientDeleteDTO,
    ClientDTO,
    ClientListDTO,
    ClientUpdateDTO,
)

router = APIRouter(prefix="/client", tags=["client"])


@router.post("/create", response_model=ClientDTO)
async def create_client(
    client: ClientCreateDTO,
    db_session=Depends(get_db_session),
) -> ClientDTO:
    """
    Create client.

    :param client: client data.
    :param db_session: database session.
    :returns: created client.
    """
    try:
        new_client = await Client.create(
            db_session=db_session,
            **client.dict(),
        )
    except SQLAlchemyError as e:
        logger.error(e.__dict__["orig"])
        return JSONResponse(
            status_code=400,
            content={"message": str(e.__dict__["orig"])},
        )
    return new_client


@router.get("/list", response_model=ClientListDTO)
async def get_clients(
    page: int = 1,
    size: int = 10,
    db_session=Depends(get_db_session),
) -> ClientListDTO:
    """
    Get clients.

    :param page: page number.
    :param size: page size.
    :param db_session: database session.
    :returns: clients list.
    """
    clients = await Client.get_list(
        db_session=db_session,
        page=page,
        size=size,
    )
    total = await Client.get_total(db_session=db_session)
    return ClientListDTO(
        data=[ClientDTO.from_orm(client) for client in clients],
        total=total,
        page=page,
        size=size,
    )


@router.get("/{client_id}", response_model=ClientDTO)
async def get_client(
    client_id: str,
    db_session=Depends(get_db_session),
) -> ClientDTO:
    """
    Get client.

    :param client_id: client id.
    :param db_session: database session.
    :returns: client.
    """
    client = await Client.get_by_id(
        db_session=db_session,
        client_id=client_id,
    )
    return ClientDTO.from_orm(client)


@router.patch("/{client_id}", response_model=ClientDTO)
async def update_client(
    client_id: str,
    client: ClientUpdateDTO,
    db_session=Depends(get_db_session),
) -> ClientDTO:
    """
    Update client.

    :param client_id: client id.
    :param client: client data.
    :param db_session: database session.
    :returns: updated client.
    """
    try:
        updated_client = await Client.update(
            db_session=db_session,
            client_id=client_id,
            **client.dict(exclude_unset=True),
        )
    except SQLAlchemyError as e:
        logger.error(e.__dict__["orig"])
        return JSONResponse(
            status_code=400,
            content={"message": str(e.__dict__["orig"])},
        )
    return updated_client


@router.delete("/{client_id}", response_model=ClientDeleteDTO)
async def delete_client(
    client_id: str,
    db_session=Depends(get_db_session),
    redis_pool: ConnectionPool = Depends(get_redis_pool),
) -> ClientDeleteDTO:
    """
    Delete client.

    :param client_id: client id.
    :param db_session: database session.
    :param redis_pool: redis connection pool.
    :returns: deleted client.
    """
    try:
        client = await Client.get_by_id(
            db_session=db_session,
            client_id=client_id,
        )
        if not client:
            return JSONResponse(
                status_code=400,
                content={"message": "Client not found"},
            )
        await Notification.delete_by_client_id(
            db_session=db_session,
            client_id=client_id,
        )
        await NotificationReceiver.delete_by_client_id(
            db_session=db_session,
            client_id=client_id,
        )
        await client.delete(db_session=db_session)
        redis: Redis = await redis_pool.acquire()
        await redis.delete(f"client:{client_id}")
        await redis_pool.release(redis)
    except SQLAlchemyError as e:
        logger.error(e.__dict__["orig"])
        return JSONResponse(
            status_code=400,
            content={"message": str(e.__dict__["orig"])},
        )
    return ClientDeleteDTO(id=client_id)
