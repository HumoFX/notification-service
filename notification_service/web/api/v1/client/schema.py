from typing import Optional

from pydantic import BaseModel


class ClientCreateDTO(BaseModel):
    """DTO for client create."""

    phone_code: int
    phone_number: int
    tag: str
    timezone: str

    class Config:
        orm_mode = True


class ClientUpdateDTO(BaseModel):
    """DTO for client update."""

    phone_code: Optional[int]
    phone_number: Optional[int]
    tag: Optional[str]
    timezone: Optional[str]

    class Config:
        orm_mode = True


class ClientDTO(BaseModel):
    """DTO for client."""

    id: str
    phone_code: int
    phone_number: int
    tag: Optional[str]
    timezone: str
    created_at: str
    updated_at: str

    @classmethod
    def from_orm(cls, client):
        return cls(
            id=str(client.id),
            phone_code=client.phone_code,
            phone_number=client.phone_number,
            tag=client.tag,
            timezone=client.timezone,
            created_at=str(client.created_at),
            updated_at=str(client.updated_at),
        )

    class Config:
        orm_mode = True


class ClientListDTO(BaseModel):
    """DTO for client list."""

    data: list[ClientDTO]
    total: int
    page: int
    size: int


class ClientDeleteDTO(BaseModel):
    """DTO for client delete."""

    id: str

    class Config:
        orm_mode = True
