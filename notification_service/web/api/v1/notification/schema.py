from datetime import datetime
from enum import Enum
from typing import Any, Optional, Type

from loguru import logger
from pydantic import BaseModel

from notification_service.settings import settings


class NotificationReceiverStatus(str, Enum):
    """Notification receiver status."""

    PENDING = "pending"
    SUCCESS = "success"
    FAILED = "failed"
    CANCELLED = "cancelled"
    DELIVERED = "delivered"
    READ = "read"


class NotificationDTO(BaseModel):
    """DTO for notification."""

    id: str
    message: str
    start_at: Optional[str]
    end_at: Optional[str]
    filtering: Optional[dict]
    created_at: str
    updated_at: str

    @classmethod
    def from_orm(cls, notification):
        return cls(
            id=str(notification.id),
            message=notification.message,
            start_at=str(notification.start_at),
            end_at=str(notification.end_at),
            filtering=notification.filtering,
            created_at=str(notification.created_at),
            updated_at=str(notification.updated_at),
        )

    class Config:
        orm_mode = True


class NotificationCreateDTO(BaseModel):
    """DTO for notification create."""

    message: str
    start_at: Optional[str]
    end_at: Optional[str]
    filtering: Optional[dict]

    @classmethod
    def validate(cls: Type["Model"], value: Any) -> "Model":
        # validate start_at and end_at if they are datetime type
        start_at = value.get("start_at")
        end_at = value.get("end_at")
        if start_at and end_at:
            # convert to datetime type
            try:
                start_at = datetime.strptime(start_at, "%Y-%m-%d %H:%M")
                end_at = datetime.strptime(end_at, "%Y-%m-%d %H:%M")
            except ValueError:
                raise ValueError(
                    "start_at and end_at must be in format YYYY-MM-DD HH:MM",
                )
            # check if start_at is before end_at
            if start_at > end_at:
                raise ValueError("start_at must be before end_at")
            tzinfo = settings.tz_info
            datetime_now = datetime.now(tz=tzinfo)
            logger.info(f"datetime_now: {datetime_now}")
            # check if start_at is before datetime now
            if start_at < datetime.strptime(
                datetime_now.strftime("%Y-%m-%d %H:%M"),
                "%Y-%m-%d %H:%M",
            ):
                raise ValueError("start_at must be after datetime now")
            cls.start_at = start_at
            cls.end_at = end_at
        return cls(**value)

    class Config:
        orm_mode = True


class NotificationUpdateDTO(BaseModel):
    """DTO for notification update."""

    message: Optional[str]
    start_at: Optional[str]
    end_at: Optional[str]
    filtering: Optional[dict]

    class Config:
        orm_mode = True


class NotificationListDTO(BaseModel):
    """DTO for notification list."""

    data: list[NotificationDTO]
    total: int
    page: int
    size: int


class NotificationReceiverDTO(BaseModel):
    """DTO for notification receiver."""

    id: str
    notification_id: str
    client_id: str
    status: str
    created_at: str
    updated_at: str

    @classmethod
    def from_orm(cls, notification_receiver):
        return cls(
            id=str(notification_receiver.id),
            notification_id=str(notification_receiver.notification_id),
            client_id=str(notification_receiver.client_id),
            status=notification_receiver.status,
            created_at=str(notification_receiver.created_at),
            updated_at=str(notification_receiver.updated_at),
        )

    class Config:
        orm_mode = True
