from fastapi import APIRouter, BackgroundTasks
from fastapi.param_functions import Depends
from fastapi.responses import JSONResponse
from redis.asyncio import Redis
from sqlalchemy.exc import SQLAlchemyError

from notification_service.db.dependencies import get_db_session
from notification_service.db.models import Notification
from notification_service.logging import logger
from notification_service.services.celery.tasks import (
    batch_notification,
    create_notification_receivers,
)
from notification_service.services.redis.dependency import get_redis_pool
from notification_service.web.api.v1.notification.schema import (
    NotificationCreateDTO,
    NotificationDTO,
    NotificationListDTO,
)

router = APIRouter(prefix="/notification", tags=["notification"])


@router.post("/create", response_model=NotificationDTO)
async def create_notification(
    background_tasks: BackgroundTasks,
    notification: NotificationCreateDTO,
    db_session=Depends(get_db_session),
    redis: Redis = Depends(get_redis_pool),
) -> NotificationDTO:
    """
    Create notification.

    :param background_tasks: background tasks.
    :param notification: notification data.
    :param db_session: database session.
    :param redis: redis connection pool.
    :returns: created notification.
    """
    try:
        new_notification = await Notification.create(
            db_session=db_session,
            **notification.dict(),
        )
        filtering = notification.filtering
        logger.info(f"filtering: {filtering}")
        if filtering:
            clients = await Notification.notification_receivers_by_filtering(
                db_session,
                filtering,
            )
            # run async task to create notification receivers in background
            background_tasks.add_task(
                create_notification_receivers,
                db_session,
                new_notification,
                clients,
            )
            expires = notification.end_at
            notification_id = str(new_notification.id)
            batch_notification.apply_async(
                args=[notification_id],
                eta=notification.start_at,
                expires=expires,
            )

    except SQLAlchemyError as e:
        logger.error(e.__dict__["orig"])
        return JSONResponse(
            status_code=400,
            content={"message": str(e.__dict__["orig"])},
        )
    return new_notification


@router.get("/list", response_model=NotificationListDTO)
async def get_notifications(
    page: int = 1,
    size: int = 10,
    db_session=Depends(get_db_session),
) -> NotificationListDTO:
    """
    Get notifications.

    :param page: page number.
    :param size: page size.
    :param db_session: database session.
    :returns: notifications list.
    """
    notifications = await Notification.get_list(
        db_session=db_session,
        page=page,
        size=size,
    )
    return notifications
