"""notification_service models."""
import pkgutil
from pathlib import Path

from notification_service.db.models.notification import (
    Notification,
    NotificationReceiver,
)
from notification_service.db.models.user import Client

__all__ = ["Client", "Notification", "NotificationReceiver", "load_all_models"]


def load_all_models() -> None:
    """Load all models from this folder."""
    package_dir = Path(__file__).resolve().parent
    modules = pkgutil.walk_packages(
        path=[str(package_dir)],
        prefix="notification_service.db.models.",
    )
    for module in modules:
        __import__(module.name)  # noqa: WPS421
