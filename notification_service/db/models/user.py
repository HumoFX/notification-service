from sqlalchemy import Column, DateTime, Integer, String, UniqueConstraint, func, select
from sqlalchemy.orm import relationship

from notification_service.db.base import Base
from notification_service.db.guid import GUID, GUID_SERVER_DEFAULT_POSTGRESQL


class Client(Base):
    """Client model."""

    __tablename__ = "client"

    id = Column(GUID, primary_key=True, server_default=GUID_SERVER_DEFAULT_POSTGRESQL)
    phone_code = Column(Integer, nullable=False)
    phone_number = Column(Integer, nullable=False)
    tag = Column(String(255), nullable=False)
    timezone = Column(String(255), nullable=False)
    created_at = Column(DateTime, nullable=False, server_default=func.now())
    updated_at = Column(DateTime, nullable=False, server_default=func.now())
    notification_receivers = relationship(
        "NotificationReceiver",
        back_populates="client",
    )

    # phone_number and phone_code should be unique together
    __table_args__ = (
        UniqueConstraint("phone_number", "phone_code", name="unique_phone_number"),
    )

    @classmethod
    async def create(cls, db_session, **kwargs):
        """Create client."""
        client = cls(**kwargs)
        db_session.add(client)
        try:
            await db_session.commit()
        except Exception as e:
            await db_session.rollback()
            raise e
        return client

    @classmethod
    async def get_by_id(cls, db_session, client_id):
        """Get client by id."""
        stmt = select(cls).filter(cls.id == client_id)
        result = await db_session.execute(stmt)
        return result.scalar()

    @classmethod
    async def update(cls, db_session, client_id, **kwargs):
        """Update client."""
        stmt = select(cls).filter(cls.id == client_id)
        result = await db_session.execute(stmt)
        result = result.scalar()
        result.updated_at = func.now()
        for key, value in kwargs.items():
            setattr(result, key, value)
        try:
            await db_session.commit()
        except Exception as e:
            await db_session.rollback()
            raise e
        return result

    @classmethod
    async def delete(cls, db_session, client_id):
        """Delete client."""
        stmt = cls.query(db_session).filter(cls.id == client_id)
        await stmt.delete()
        await db_session.flush()

    @classmethod
    async def get_by_phone_number(cls, db_session, phone_number, phone_code):
        """Get client by phone_number."""
        stmt = cls.query(db_session).filter(
            cls.phone_number == phone_number,
            cls.phone_code == phone_code,
        )
        return await stmt.first()

    @classmethod
    async def get_list(cls, db_session, page, size):
        """Get clients list."""
        stmt = select(cls).offset((page - 1) * size).limit(size)
        result = await db_session.execute(stmt)
        return result.scalars().all()

    @classmethod
    async def get_total(cls, db_session):
        """Get total clients count."""
        stmt = select(func.count(cls.id))
        result = await db_session.execute(stmt)
        return result.scalar()

    @property
    def full_phone_number(self):
        return f"{self.phone_code}{self.phone_number}"
