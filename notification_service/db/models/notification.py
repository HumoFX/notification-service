from datetime import datetime

from sqlalchemy import JSON, Column, DateTime, ForeignKey, String, func, select
from sqlalchemy.orm import relationship

from notification_service.db.base import Base
from notification_service.db.guid import GUID, GUID_SERVER_DEFAULT_POSTGRESQL
from notification_service.db.models.user import Client


class Notification(Base):
    """Notification model."""

    __tablename__ = "notification"

    id = Column(GUID, primary_key=True, server_default=GUID_SERVER_DEFAULT_POSTGRESQL)
    message = Column(String(255), nullable=False)
    start_at = Column(DateTime, nullable=True)
    end_at = Column(DateTime, nullable=True)
    filtering = Column(JSON, nullable=True)
    created_at = Column(DateTime, nullable=False, server_default=func.now())
    updated_at = Column(DateTime, nullable=False, server_default=func.now())
    notification_receivers = relationship(
        "NotificationReceiver",
        back_populates="notification",
    )

    @classmethod
    async def create(cls, db_session, **kwargs):
        """Create notification."""
        start_at = kwargs.get("start_at")
        end_at = kwargs.get("end_at")
        if start_at:
            kwargs["start_at"] = datetime.strptime(start_at, "%Y-%m-%d %H:%M")
        if end_at:
            kwargs["end_at"] = datetime.strptime(end_at, "%Y-%m-%d %H:%M")
        notification = cls(**kwargs)
        filtering = kwargs.get("filtering")
        db_session.add(notification)
        try:
            await db_session.commit()
        except Exception as e:
            await db_session.rollback()
            raise e
        return notification

    @classmethod
    async def notification_receivers_by_filtering(cls, db_session, filtering):
        """Get notification receivers by filtering."""

        tag = filtering.get("tag")
        phone_code = filtering.get("phone_code")
        stmt = select(Client)
        if tag:
            stmt = stmt.filter(Client.tag == tag)
        if phone_code:
            stmt = stmt.filter(Client.phone_code == phone_code)
        client_result = await db_session.execute(stmt)
        clients = client_result.scalars().all()
        return clients

    @classmethod
    async def get_by_id(cls, db_session, notification_id):
        """Get notification by id."""
        stmt = select(cls).filter(cls.id == notification_id)
        result = await db_session.execute(stmt)
        return result.scalar()

    @classmethod
    async def update(cls, db_session, notification_id, **kwargs):
        """Update notification."""
        stmt = select(cls).filter(cls.id == notification_id)
        result = await db_session.execute(stmt)
        result = result.scalar()
        result.updated_at = func.now()
        for key, value in kwargs.items():
            setattr(result, key, value)
        try:
            await db_session.commit()
        except Exception as e:
            await db_session.rollback()
            raise e

    @classmethod
    async def delete(cls, db_session, notification_id):
        """Delete notification."""
        stmt = select(cls).filter(cls.id == notification_id)
        result = await db_session.execute(stmt)
        result = result.scalar()
        db_session.delete(result)
        try:
            await db_session.commit()
        except Exception as e:
            await db_session.rollback()
            raise e


class NotificationReceiver(Base):
    """NotificationReceiver model."""

    __tablename__ = "notification_receiver"

    id = Column(GUID, primary_key=True, server_default=GUID_SERVER_DEFAULT_POSTGRESQL)
    notification_id = Column(GUID, ForeignKey("notification.id"), nullable=False)
    client_id = Column(GUID, ForeignKey("client.id"), nullable=False)
    status = Column(String(255), nullable=False)
    created_at = Column(DateTime, nullable=False, server_default=func.now())
    updated_at = Column(DateTime, nullable=False, server_default=func.now())
    notification = relationship("Notification", back_populates="notification_receivers")
    client = relationship("Client", back_populates="notification_receivers")

    @property
    def phone(self):
        """Get client phone."""
        return int(self.client.full_phone_number)
