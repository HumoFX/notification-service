"""empty message

Revision ID: f37c3495558b
Revises: 819cbf6e030b
Create Date: 2023-07-21 13:09:16.398739

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "f37c3495558b"
down_revision = "819cbf6e030b"
branch_labels = None
depends_on = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    import notification_service

    op.create_table(
        "client",
        sa.Column(
            "id",
            notification_service.db.guid.GUID(),
            server_default=sa.text("gen_random_uuid()"),
            nullable=False,
        ),
        sa.Column("phone_code", sa.Integer(), nullable=False),
        sa.Column("phone_number", sa.Integer(), nullable=False),
        sa.Column("tag", sa.String(length=255), nullable=False),
        sa.Column("timezone", sa.String(length=255), nullable=False),
        sa.Column(
            "created_at",
            sa.DateTime(),
            server_default=sa.text("now()"),
            nullable=False,
        ),
        sa.Column(
            "updated_at",
            sa.DateTime(),
            server_default=sa.text("now()"),
            nullable=False,
        ),
        sa.PrimaryKeyConstraint("id"),
        sa.UniqueConstraint("phone_number", "phone_code", name="unique_phone_number"),
    )
    op.create_table(
        "notification",
        sa.Column(
            "id",
            notification_service.db.guid.GUID(),
            server_default=sa.text("gen_random_uuid()"),
            nullable=False,
        ),
        sa.Column("message", sa.String(length=255), nullable=False),
        sa.Column("start_at", sa.DateTime(), nullable=True),
        sa.Column("end_at", sa.DateTime(), nullable=True),
        sa.Column("filtering", sa.JSON(), nullable=True),
        sa.Column(
            "created_at",
            sa.DateTime(),
            server_default=sa.text("now()"),
            nullable=False,
        ),
        sa.Column(
            "updated_at",
            sa.DateTime(),
            server_default=sa.text("now()"),
            nullable=False,
        ),
        sa.PrimaryKeyConstraint("id"),
    )
    op.create_table(
        "notification_receiver",
        sa.Column(
            "id",
            notification_service.db.guid.GUID(),
            server_default=sa.text("gen_random_uuid()"),
            nullable=False,
        ),
        sa.Column(
            "notification_id",
            notification_service.db.guid.GUID(),
            nullable=False,
        ),
        sa.Column("client_id", notification_service.db.guid.GUID(), nullable=False),
        sa.Column("status", sa.String(length=255), nullable=False),
        sa.Column(
            "created_at",
            sa.DateTime(),
            server_default=sa.text("now()"),
            nullable=False,
        ),
        sa.Column(
            "updated_at",
            sa.DateTime(),
            server_default=sa.text("now()"),
            nullable=False,
        ),
        sa.ForeignKeyConstraint(["client_id"], ["client.id"]),
        sa.ForeignKeyConstraint(["notification_id"], ["notification.id"]),
        sa.PrimaryKeyConstraint("id"),
    )
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table("notification_receiver")
    op.drop_table("notification")
    op.drop_table("client")
    # ### end Alembic commands ###
