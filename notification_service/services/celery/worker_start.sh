#!/bin/bash
set -o errexit
set -o nounset
ls
celery -A notification_service.services.celery.dependencies.celery_app worker -l INFO
