import asyncio
import json
import uuid
from datetime import datetime

from celery import shared_task
from loguru import logger
from sqlalchemy import select
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.orm import selectinload

from notification_service.db.models.notification import NotificationReceiver
from notification_service.services.celery.dependencies import get_celery_app
from notification_service.services.redis.dependency import redis

celery_app = get_celery_app()


def get_or_create_loop():
    try:
        return asyncio.get_running_loop()
    except RuntimeError:
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        return loop


async def create_notification_receivers(db_session, notification, clients):
    """
    Create notification receivers.Why do we need to create notification receivers in a separate task?
    Because we need to create notification receivers in a separate transaction not to block the main transaction.
    There may be a lot of clients, and it will take a long time to create notification receivers.

    :param db_session: database session.
    :param notification: notification.
    :param clients: clients list.
    """
    logger.info(f"db_session: {db_session}")
    try:
        for client in clients:
            new_notification_receiver = NotificationReceiver(
                notification_id=notification.id,
                client_id=client.id,
                status="created",
            )
            db_session.add(new_notification_receiver)
        db_session.commit()
    except SQLAlchemyError as e:
        db_session.rollback()
        logger.error(e.__dict__["orig"])
        raise e
    query = (
        select(NotificationReceiver)
        .options(
            selectinload(NotificationReceiver.notification),
            selectinload(NotificationReceiver.client),
        )
        .where(
            NotificationReceiver.notification_id == notification.id,
        )
    )
    notification_receivers = await db_session.scalars(query)
    notification_receiver_ids = [
        (
            notification_receiver.id,
            notification_receiver.phone,
        )
        for notification_receiver in notification_receivers
    ]
    notification_receiver_dict = dict(
        (str(x), str(y)) for x, y in notification_receiver_ids
    )
    logger.info(f"notification_receiver_ids: {notification_receiver_ids}")
    data = {
        "notification_receivers": notification_receiver_dict,
        "notification_message": notification.message,
    }
    expire_at = (notification.end_at - datetime.now()).total_seconds()
    data = json.dumps(data)
    await redis.set(
        f"notification:{notification.id}",
        data,
        ex=int(expire_at),
    )
    logger.info(f"redis data: {data}")
    return True


async def get_notification_data(notification_id):
    """
    Get notification data from redis.

    :param notification_id: notification id.
    :returns: notification data.
    """
    data = await redis.get(f"notification:{notification_id}")
    if data:
        data = json.loads(data)
        return data
    return None


@shared_task(
    name="batch_notification",
    bind=True,
    max_retries=3,
    default_retry_delay=10,
    ignore_result=True,
)
def batch_notification(self, notification_id):
    """
    Send batch notification to clients.

        :param self: celery task.
    :param notification_id: notification id.
    """
    self.update_state(state="PROGRESS", meta={"status": "started"})
    logger.info(f"Start send batch notification with id {notification_id} to clients")
    loop = get_or_create_loop()
    logger.info(f"Send batch notification with id {notification_id} to clients started")
    data = loop.run_until_complete(get_notification_data(notification_id))
    if data:
        notification_receivers: dict = data["notification_receivers"]
        notification_list = [(k, v) for k, v in notification_receivers.items()]
        notification_message = data["notification_message"]
        batch_size = 100
        tasks = []
        for i in range(0, len(notification_list), batch_size):
            for notification_receiver in notification_list[i : i + batch_size]:
                tasks.append(
                    (
                        notification_receiver[0],
                        notification_receiver[1],
                        notification_message,
                    ),
                )
        for task in tasks:
            loop.run_until_complete(send_notification_to_client(*task))
        logger.info(f"Send batch notification with id {notification_id} to clients end")


@celery_app.task
async def send_notification(notification_receivers):
    """
    Send notification to client.

    :param notification_receivers: notification receivers.
    """
    for notification_receiver in notification_receivers:
        logger.info(
            f"Send notification to client {notification_receiver.client_id} in "
            f"{notification_receiver.notification_id} "
            f"with message {notification_receiver.notification.message}",
        )


async def send_notification_to_client(notification_receiver_id, phone, message):
    """
    Send notification to client.

    :param notification_receiver_id: notification receiver id.
    :param phone: client phone.
    :param message: notification message.

    # :param redis: redis connection.
    """
    logger.info(f"NID {notification_receiver_id} phone {phone} message {message}")
    msg_id = uuid.UUID(notification_receiver_id)
    msg_id = msg_id.int
    async with aiohttp.ClientSession() as session:
        url = settings.notification_api_url + "/send/{}".format(
            msg_id,
        )
        headers = {
            "Content-Type": "application/json",
            "Authorization": "Bearer {}".format(settings.notification_api_token),
        }
        rand_id = uuid.uuid4().int

        payload = {
            "id": rand_id,
            "phone": phone,
            "text": message,
        }

        async with session.post(url, headers=headers, json=payload) as response:
            if response.status == 200:
                if await response.json():
                    await redis.set(
                        f"notification:{notification_id}:success",
                        client.id,
                    )
                    logger.info(
                        f"Add client {notification_receiver[1]} to notification {notification_receiver[2]}",
                    )
                    return await response.json()
                else:
                    await redis.set(
                        f"notification:{notification_id}:fail",
                        client.id,
                    )
                    logger.info(
                        f"{notification_receiver[1]} to notification {notification_receiver[2]}",
                    )
                    return None
            logger.info(f"response status {response.status}")
            return None
