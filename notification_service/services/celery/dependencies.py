from celery.app import Celery

from notification_service.settings import settings


def get_celery_app() -> Celery:
    """
    Get celery app.

    :return: celery app.
    """
    return Celery(
        "notification_service",
        broker=str(settings.rabbit_url),
        backend=str(settings.redis_url),
        include=[
            "notification_service.services.celery.tasks",
        ],
    )


celery_app = get_celery_app()
