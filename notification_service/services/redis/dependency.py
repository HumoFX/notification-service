from typing import AsyncGenerator

from redis.asyncio import Redis
from starlette.requests import Request

from notification_service.settings import settings


async def get_redis_pool(
    request: Request = None,
) -> AsyncGenerator[Redis, None]:  # pragma: no cover
    """
    Get redis connection pool.
    :param request: current request.
    :returns:  redis connection pool.
    """
    return request.app.state.redis_pool


redis = Redis.from_url(str(settings.redis_url))
