# notification_service
## Poetry

В приложении используется [poetry](https://python-poetry.org/).



## Запуск приложения
```bash
docker-compose -f deploy/docker-compose.yml --project-directory . build
```

## Конфигурация

Можно задать переменные окружения для конфигурации приложения в файле `.env` в корне проекта.(сам не выводил переменные, по времени не успел)
Но я бы рекомендовал использовать переменные окружения в системе.(см. [12factor](https://12factor.net/ru/config))
## Миграции

Для миграций используется [alembic](https://alembic.sqlalchemy.org/en/latest/).
```bash
# To run all migrations until the migration with revision_id.
alembic upgrade "<revision_id>"

# To perform all pending migrations.
alembic upgrade "head"
```

### Вернуть миграции

Что бы откатить миграции используйте:
```bash
# revert all migrations up to: revision_id.
alembic downgrade <revision_id>

# Revert everything.
 alembic downgrade base
```

### Генерация миграций

Для генерации миграций используйте:
```bash
# For automatic change detection.
alembic revision --autogenerate

# For empty file generation.
alembic revision
```


## Для запуска тестов

Для запуска тестов используйте:
P.S. Не успел сделать тесты, но вроде как должно работать

```bash
docker-compose -f deploy/docker-compose.yml -f deploy/docker-compose.dev.yml --project-directory . run --build --rm api pytest -vv .
docker-compose -f deploy/docker-compose.yml -f deploy/docker-compose.dev.yml --project-directory . down
```

# Дополнительно

```
- [-] Добавить тесты(не успел сделать)
- [*] Добавить логирование(на мой взгляд логирование должно быть везде, но я не успел его добавить)
- [-] Добавить мониторинг (не успел сделать)
- [*] Добавить документацию к API /docs
- [*] Добавить валидацию входных данных
- [*] Добавить миграции
- [*] Добавить docker-compose
- [*] Добавить dockerfile
- [*] Добавить CI/CD
- [*] Добавить конфигурацию
- [*] Добавить линтеры, форматтеры
- [*] Добавить pre-commit
- [-] Добавить kubernetes(не успел настроить)
- [-] OAuth2 авторизация
- [*] WebUI для отправки уведомлений от Flower
```

## Тестовое задание очень понравилось, но не успел сделать все, что хотелось бы.
## Буду рад обратной связи. Спасибо за внимание!
