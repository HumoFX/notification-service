#!/bin/bash
#set -o errexit
#set -o nounset
# get the celery app from the dependencies module and start a worker
# not seeing notification_service.services.celery.dependencies.get_celery_app
# so using notification_service.services.celery.dependencies.get_celery_app
#celery dependencies.get_celery_app worker -l INFO

celery -A notification_service.services.celery.dependencies.get_celery_app worker -l INFO
